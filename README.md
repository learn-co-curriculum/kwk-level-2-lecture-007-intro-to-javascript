# Intro to JavaScript

**NOTE:** This is a long lecture. Make sure to provide mini breaks, show examples in code constantly, re-iterate previous points made, and explore students questions by asking the class to participate in finding the answers

## Objectives

This lecture is geared around describing the JavaScript language and its many uses in general terms. It makes use of the Chrome Developer Tools to write some JavaScript code (an alternative to running it in an interactive Node instance). We will introduce functions and practice writing the basics of them.

## SWBATS

+ JAVASCRIPT - use and describe variables
+ JAVASCRIPT - identify the common data types of JavaScript
+ JAVASCRIPT - write JS functions with correct syntax
+ JAVASCRIPT - use the Chrome Developer Tools to define and call functions
+ JAVASCRIPT - explore some common built in functions such as `console.log()` and `alert()`

## INTRODUCTION

_JavaScript_, commonly abbreviated as _JS_, is the programming language of the
web. Along with HTML and CSS, it's used to create everything from Google to
Facebook to Wikipedia to Amazon to Netflix.

When you look at the content on a website, each of the three main web
technologies plays a role:

- HyperText Markup Language (**HTML**) provides structure to the content, arranging it in a nested, tree-like manner
- Cascading Style Sheets (**CSS**) adds styling to the page, letting us customize the look of text content, the background color of the page, and the size and position of the various HTML elements
- **JavaScript** does pretty much everything else, handling most of the dynamic behavior of a website. For example, reacting to user events (like clicking a button), changing the page's content without reloading, and executing network requests in the background is all the responsibility of JavaScript

When you start typing in a Google search and it auto-populates the rest of the query, that's JavaScript.

When you click on a navigation button and a drop down menu slides into view,
that's JavaScript.

When you see new notification alerts on Twitter without refreshing the page,
that's JavaScript.

When new content progressively appears at the bottom of the page as you scroll
down your Facebook news feed, that's _also JavaScript!_

JavaScript has advanced over the years and continues to be the most critical
programming language of the internet.  Learning just a bit of JS will give you a lot of power over how a website interacts with its users.

In this lesson, we will be starting off with the basics: what variables and
functions are and how to write them. But before we begin, let's do a quick crash course in JavaScript as a scripting language.

## JavaScript

The name _JavaScript_ was chosen as a marketing ploy: the _Java_ language was
all the rage in 1995, but the two languages actually share very little in common.
Some JS syntax is borrowed from Java (to make it more familiar for those early
developers coming over from the world of Java), but that's about it. Now, Java and JavaScript are as similar as car and carpet.

JavaScript is a scripting language and is also _interpreted_ rather than _compiled_. Without getting too in the weeds about further defining either of those statements, this ultimately means JavaScript has barriers to rapid development and deployment removed. We can easily write some code, run it in multiple environments (think your computer, the browser, etc.), and edit it. 

Because it doesn't get compiled, we can poke around, test things, make quick changes and test
again, etc... With only a little bit of training, you'll find yourself able to go to a website
you like, open up the source code using your browser's developer tools, and
**look directly at all of the JavaScript code used on that site**. This is an
extremely powerful feature of the language and an incredible tool for learning
by example.

**NOTE:** As a quick student empowerment challenge, ask them to open up Chrome Developer Tools on some website of their choosing. Following, ask them to see if they can't locate the JavaScript source code that the website is using (they are in the sources tab).

**NOTE:** For the following section, have students open up Chrome to a new tab, then open the Chrome developer tools. 
They should follow along by typing in any of the examples
during this lecture to see for themselves how variables and functions work. Remind them that the console in Chrome interprets and runs JavaScript.

## Variables

A variable, put simply, is a container in which we can store values for later
retrieval, i.e.: `var x = 3`

Imagine a box that can hold any type of data: a number, a word, a boolean
(`true`/`false`), a collection of values, etc.. We take some data that we want
to store, place it inside the box, and hand the box off to the JavaScript
engine, which stores it in our computer's memory. All done! Our data is safely
stored until we need to access it again.

But wait! How will we ask for it back once it's stored? This is why we need to
assign a name to our variable — a label for our box — so that we can tell JS
exactly which piece of stored data we want to access.

To do this, we declare a variable by first write `var` to tell JS this is a
variable, then by giving it a name, like so:

```js
var myVariable
```

This will tell JS to create a 'box' named 'myVariable'.  This alone will work.
In your developer console, if you run the above example, you will create a spot
in memory that JS will label  `myVariable`.  If we wanted to retrieve this
information, all we need to do is, on a new line in the developer console, write
`myVariable`, and JS will return the value of that variable.

So far, though, if we run `myVariable` in console, we get `'undefined'`. Why is
this the case? Well, `myVariable` is undefined because all we've done so far is
create the box and label: we haven't put anything in just yet!

To store a value in a variable, we write `var myVariable` just like before, but
assign a value to it like so:

**NOTE:** when reading code like this, do your best to use the term 'gets' instead of 'equals' in the below statement. As you know, in JavaScript, 'equals' translates to `==` or even better `===`. The term 'gets' implies **assignment** instead of **equality**.

```js
var myVariable = "hot dogs"
```

_Now_, fire the above line off in the console. After the line is fired, enter the
variable name `myVariable` into the console.  It will return `"hot dogs"`.
You've just created your first variable and assigned it a value! You can
_reassign_ this variable if you'd prefer to store a different value.  Once
declared, a variable doesn't need `var` before it to be reassigned:

```js
var myVariable = "hot dogs" // assigns "hot dogs"
myVariable 
// > "hot dogs"

myVariable = "veggie burgers" // reassigns the variable to "veggie burgers"
myVariable
// > "veggie burgers"
```

Notice above that the lines are read in order from top to bottom.  The first
line declares a variable and assigns it a value.  If that variable is accessed
after, it will return the assigned value.  However, if the variable is
reassigned after, and accessed again, the value will have changed.

You can name variables whatever you want, though there are some [reserved
words](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Reserved_keywords_as_of_ECMAScript_2015)
that won't work.  You should _always_ aim to provide your variables with short, descriptive names that communicate what they represent/what they hold.

JavaScript has a few different ways to label variables. For the most part, they operate very similarly with only a few differences. Following is a high level overview of them:

#### `var`, `let` and `const`

##### `var`

There are three ways to declare variables, and each behaves a little differently.  We've seen the first one, `var`:

```js
var myFavoriteFood = "sushi"
```

For the purposes of this course, using `var` will work almost all of the time. By assigning a variable using `var`, we can change variables like we've seen, so if we wanted to change `myFavoriteFood` to equal `grilled cheese` after its been declared, it's simply:

```js
var myFavoriteFood = "sushi" // assigns "sushi" to the variable named myFavoriteFood
myFavoriteFood = "grilled cheese" // reassigns the variable myFavoriteFood, so it now equals "grilled cheese"
```

##### `const`

Then there is `const`.  `const` is short for _constant_, and allows you to assign a variable, but _not reassign it_.  So, for instance:

```js
const myFavoriteFood = "sushi" // assigns "sushi" to the variable named myFavoriteFood
myFavoriteFood = "grilled cheese" 
// TypeError: Assignment to constant variable.
```

Once you've declared something using `const`, you can't change it. This is helpful for when you need to store some information that you do not want to ever change. For instance, if we were writing a program that involved some math, we could declare a constant variable for Pi:

```js
const PI = 3.141592653589793
```

By definition, Pi is a specific value, so we _really_ don't want to change it. Doing so runs the risk of messing up all the math that relies on the constant!

##### `let`

Then there is `let`:

```js
let myFavoriteFood = "sushi" // assigns "sushi" to the variable named myFavoriteFood
myFavoriteFood = "grilled cheese" // reassigns the variable myFavoriteFood, so it now equals "grilled cheese"
```

Huh... `let` works the _same way_ as `var`. You can assign and reassign without error, so what is the difference? Well, variables declared with `let` are only accessible within the context, or _scope_ that we've declared them.

**Note:** In terminal, run `node`.  This will start an interactive JavaScript program. Paste in the below code.  You will see the message, "My name is Beyoncé, my favorite food is grilled cheese and my favorite color is green" appear in the console.  While node is still running, try to call the variables `myName`, `myFavoriteFood` and `myFavoriteColor`. 

```js
var myName = "Beyoncé" // this variable is available everywhere

{
  let myFavoriteFood = "grilled cheese" // this variable is only available inside the curly braces
  var myFavoriteColor = "green" // this variable is available everywhere
  console.log("My name is " + myName + ", my favorite food is " + myFavoriteFood + " and my favorite color is " + myFavoriteColor)
  // in here, we have access to the variables myName, myFavoriteFood and myFavoriteColor
}
  // out here, we only have access to the variables myName and myFavoriteColor
```

In the above code, everything inside the curly braces is in a different _scope_. Trying to call the variables `myName` and `myFavoriteColor` will return their values as expected, but `myFavoriteFood` _will not!_ This is because `myFavoriteFood` was declared using `let`, meaning the variable is only available within the curly braces.  The other two variables are accessible in and out of the curly braces.  We will see curly braces whenever we write functions and loops, so just remember that if you use `let` within any curlies, that variable is only available inside of them. 

Don't worry too much about which variable you should use. Just be aware that there may be some upcoming labs that refer specifically to `let` or `const`. When in doubt, use `var` for now unless a test is specifically looking for something like a constant.  

### Types of Variables

#### Strings

So far, we've been assigning variables with word values like "hot dogs".  These
values are called **Strings**.  Strings are how we represent text in JS, and
will always be wrapped in quotes. We can store words, phrases, and anything that
can be written as text as Strings.

#### Numbers

Besides Strings, we can also store values like _numbers_ in variables:

```js
var myNewVariable = 7
```

Numbers are a different _data type_ than Strings.  You can do math with numbers,
and _since variables just represent values_, we can do math with variables as well! For now, it is reasonable to think of variables simply as aliases for the values they represent:

```js
var firstNumber = 10 //assigns the 'firstNumber' to 10
var secondNumber = 5 //assigns the 'secondNumber' to 5
firstNumber + secondNumber
// > 15
firstNumber - secondNumber
// > 5
firstNumber * secondNumber
// > 50
firstNumber / secondNumber
// > 2

// and just for funsies, let's bust out the modulo operator!
firstNumber % secondNumber
// > 0
```

Cool! But not that useful just yet.  Why store numbers in variables if we
can just do math with the numbers directly? `10 + 5` will return the same result
as `firstNumber + secondNumber` in the example above. Well, often we
reassign variables (afterall, it's their namesake!), or maybe we don't know what each variable value actually is at
a given moment. 

Maybe all we know is that we have two variables, representing numbers, and we want the result of adding them together. The line
`firstNumber + secondNumber` will always add the two variables, but may return a
different result if those variables are reassigned to different numbers, whereas `10 +
5` will _always_ return 15. This dynamic behavior is important for writing
abstract code. 

Storing number values is also useful for keeping the results of complex math
equations:

```js
var complexResult = (10 * 413 + 40244 / (2 - 45)) * 67
complexResult
// > 214004.23255813954

// or without using constant values_
var tax = (itemPrice + shipping) * stateTax
var total = itemPrice + tax
```

If we were writing a program that needed to perform the same equation multiple
times, we could just perform it once, as we did above, and then call the
variable later on whenever we needed that result!

**NOTE:** Give students a brief challenge - they should create a couple of variables and try working with them together:
  - Create a variable to store for the first few digits of _pi_
  - Create a second variable to store any number that represents to the radius of a circle
  - Have students write math equations using these two variables that will output the _circumference_ (2 \* pi \* radius) and _area_ of a circle (pi \* radius \* radius)

#### Booleans

**NOTE:** As a brief aside, let's jazz the student's up on what could be considered the most simple and dry programming concept ever: booleans.  Booleans represent the most basic, most granular unit of information that we know of _in the universe_. We interact with them mostly as `true` and `false` when used as logical operators, but you can also equate booleans to either on or off, one or zero, existing or not existing. In fact, when we see binary in movie tropes (the zeroes and ones flying across the screen), all that really represents is the computer saying "true false true true false etc.". _What_ it's saying true (1) and false (0) about is whether or not a very small physical spot in memory (think hard drive) is magnetized or not. 

Booleans are a special type of value - they _only_ ever equal **true** or
**false**, and nothing else. Boolean variables are great when you need a to do
things _conditionally_.  

Imagine the summer is over, its the morning and you're about to go outside and
head to school. You look out the window to check the weather before leaving.
_If_ it is raining out, you will take an umbrella. In JavaScript, we could
represent the weather with a variable such as:

```js
var isRaining = false
```

Great! It isn't raining.  Based on this info, we can plan to do things
differently - we'll grab some sunglasses and sandals and hit the streets. If,
however...

```js
var isRaining = true
```

 We'll know that its time to grab the umbrella, put on some boots and a rain
coat, and cancel our plans to have lunch in the park. While we are getting
ahead of ourselves with functions here, let's just take a quick look at how
this is expressed programmatically: 

```js
if (isRaining) {
  "bring umbrella"
} else {
  "wear sandals"
}
```

We'll go more into conditionals later on, but it is good to be aware that these
are another type of data type we can store in a variable.

### There are more variables!

There are additional data types, such as objects, but we won't cover them just
yet for the sake of simplicity.  What we have covered so far (booleans, numbers,
strings), are enough for us to approach the next topic of this lecture,
_functions_. 

**NOTE:** It may be good to let students take a five minute break before diving
into functions.

## Functions

Functions are the single most important unit of code in JavaScript. From now on,
almost all of the JavaScript code that we write will be enclosed in a function.
A function is repeatable code. A subroutine. It is behavior we can define and
execute as many times as we want at a later point. 

Imagine we had chores to do, which may not be that hard to imagine :(. We need to:

  - Wash the dishes
  - Clean our bedroom

When we state we're going to do these things, we are referring to many _actions_
that will be taken when we're actually doing the chores. Really, though, these
chores can be broken down further to smaller actions:

  - Wash the dishes
    1. Turn on faucet
    2. Add soap to sponge
    3. Take dish out of sink
    4. Scrub dish with sponge
    5. Wash dish with running water
    6. Place dish in drying rack
    7. Repeat until sink does not have dishes to clean
    
    
  - Clean our bedrooms
    1. Turn light on
    2. Make bed
    3. Fold clean clothes
    4. Put clean clothes away
    5. Pick up dirty clothes
    6. Place dirty clothes in pile for laundry
    7. Throw away trash
    ...etc, etc, etc...

Each step listed is a distinct thing to do.  By definition, washing dishes
means to do each thing within.  When we _do_ the dishes, we follow these steps.

Functions can be thought of in a similar way.  Functions are _actions_.  Writing
a function is writing a definition of what that action is. Calling the function
is _doing_ that action and all the steps involved.

### Structure of a Function

```js
function washDishes() {
  //all the steps to washing dishes
}
```

Every function is defined first by writing the word `function`, (just like we
write `var` before defining a variable!). We then give a name to the function:
`washDishes`, for instance. The name is followed by a set of parentheses, `()`.
These are also required for JS to understand that this is a function definition.
After the parentheses, we add a set of curly braces, `{}`. Everything inside
these curly braces makes up the definition of the function.

##### Using Functions

Once we have a function defined, we will want to make use of, or execute, it. We
call this either "calling" or "invoking" a function. A function can return a
value. This isn't always the case, but often, when we call a function, we do it
because we expect a result. For instance, we might write a function that returns
the sum of two numbers, like so:

```js
function addTwoNumbers() {
  var result = 10 + 5
  return result
}
```

`addTwoNumbers()` takes two steps here:
  * it declares a variable `result`, and assigns it the value of `10 + 5`
  * it returns the value that `result` represents, in this case `15`

If we were to call this function, similar to variables, we just need to write
the _name_ of the function, followed by those parentheses, to call it:

```js
addTwoNumbers() // the function we defined is called.
```

Now, similar to a variable, writing `addTwoNumbers()` on a line _represents_
whatever value it returned after it took its steps.  We can actually write
something like this and JavaScript will get the correct result:

```js
addTwoNumbers() + 20 // this is the equivalent of 15 + 20, or 35
```

...logically it follows that we can capture the result of functions in variables...

```js
var x = addTwoNumbers()

// computationally the same thing as doing:
// var x = 10 + 5
```

Pretty neat, huh? Functions not only take a series of actions _we define_, but
they also represent the returned result of those actions when called in a line
of code.  

##### Arguments and Parameters

**NOTE:** For now, tell them to assume the two are synonymous. Once they understand the concept of an argument, illuminate the difference: arguments are what we _pass_ to functions when calling them, while parameters are what we write in the function _definitions_. Show this with a code example of defining and calling a function. 

There is one more thing about function structure we have to talk about! _Arguments and Parameters_.  

![Arguing](https://s3.amazonaws.com/ironboard-learn/argument.png)

No... not that kind.  

Let's modify the function we wrote in the last section:

```js
// before
function addTwoNumbers() {
  var result = 10 + 5
  return result
}

// instead, let's use:
function addFive(x) {
  var result = x + 5
  return result
}

```

With this minor change, we can see the power of functions: we don't actually
need to know all the values when we write them.  We can write more _general
purpose_ functions this way that don't necessarily always return the same result -
like a function that adds '5' to _any other number_ that you give to it, or a
function that multiplies any number by itself.  We do this by passing
_arguments_ into a function:

```js
function addFive(anyNumber) { // anyNumber is the parameter
  var result = anyNumber + 5
  //the function now returns the result of anyNumber + 5
  return result
}

addFive(10)
// > 15
addFive(20) 
// > 20
addFive(5) 
// > 10
```

Now our function is a bit more dynamic; it will return different results
depending on what is passed into the function as an argument.  Functions can
have as many arguments as you like, though we'd recommend trying to use a small
amount for each function.  Here are a few more examples of functions with
arguments:

```js
function squareNumber(anyNumber) {
  var result = anyNumber * anyNumber // 2 * 2 = 4, 3 * 3 = 9, 4 * 4 = 16, etc...
  return result
}

function pluralize(anyString) {
  var result = anyString + "s" //huh, that is interesting, isn't it?
  return result
}

function multiplyTwoNumbers(firstNumber, secondNumber) {
  return firstNumber * secondNumber // we don't actually have to declare a result variable. This is valid
}

squareNumber(4) //
// > 16
squareNumber(5) //
// > 25
squareNumber(144) //
// > 20736
squareNumber(1.5) //
// > 2.25

pluralize("hot dog") 
// > "hot dogs"
pluralize("cat") 
// > "cats"
pluralize("student") 
// > "students"
pluralize(5) 
// > "5s" !!! the number converted to a String! Weird!

multiplyTwoNumbers(5, 10) 
// > 50
multiplyTwoNumbers(10, 10) 
// > 100
multiplyTwoNumbers(14423, 1322) 
// > 19067206
```

**Note:** Have students break out in to small groups and write functions for practice. Stick to math for now, but students can:
 * try different amounts of arguments
 * try passing in variables as arguments
 * see what happens when they don't give all the arguments a function expects
 * see what happens if we give too many arguments
 * see what a function returns if there is no `return` line in its definition

### Built In JavaScript Functions

JavaScript comes with many preexisting functions we can use. Two common built in
functions we will use are `console.log()` and `alert()`.  Notice that these too
have parentheses at the end.  Remember, when JavaScript reads code with `()` at the end, it attempts to execute that code as a function. 

#### `alert()`

**NOTE:** In the Chrome developer console, have students first write `alert`.
The console will respond with the _definition_ of `alert`, which shows up as `ƒ
alert() { [native code] }` (it says native code because this is a _built in_
function). Now, have the students add parentheses and run `alert()`. A pop-up
will appear! Depending on the site they're on, they may see something like
"learn.co says", or "www.google.com says".  

With `alert()`, we can tell our browser to open a pop-up window. Cool, but there
isn't a message! To add a message, you must pass in an argument.

Try passing in a String! It will display the string
Now, try passing in 5 + 5. It will display the _resulting value_: `10`. When functions like `alert()` and `console.log()` execute, they will usually attempt to convert their argument to a string. 

Now, what happens if we use `alert()` in one of our example functions?

```js
function pluralize(anyString) {
  var result = anyString + "s" //adds "s" to the argument, anyString
  alert(result) // pop-up window displays the result
}

pluralize("hot dog") // pop-up window will say "hot dogs"
pluralize("cat") // pop-up window will say "cats"
pluralize("student") // pop-up window will say "students"
pluralize(5) // pop-up window will say "5s"
```

Notice, here is an example of a function where we don't really need to return
any value, since all we're doing is sending an alert.

Notice, _too_, that `alert()` is a function that we've called _inside_ another
function. We'll come back to this later.

#### `console.log()`

Similar to `alert()`, `console.log()` outputs whatever is passed into it as an
argument.  Instead of a pop-up, however, it displays in the dev console.  We
can modify our last example:

```js
function pluralize(anyString) {
  var result = anyString + "s" //adds "s" to the argument, anyString
  console.log(result) // console displays the result
}
```

Pop-up alerts are useful for when we REALLY NEED to tell a user something.
Using `console.log()` is less disruptive and not intended for users.
`console.log()` is very useful for testing, as it allows us to see what is
happening inside of our functions as they fire. Take a look at this function,
for example:

```js
function addingNumbers(num) {
  var result = num + 10
  console.log(result)
  var secondResult = result + num
  console.log(secondResult)
  var thirdResult = result + secondResult
  console.log(thirdResult)
}

addingNumbers(4)
// > 14
// > 18
// > 32
```

Functions can get really complex, really fast, and sometimes it is hard to know
_exactly_ what is going on, especially once we start using a lot of variables.
For this reason, `console.log()` is a very, very important tool in our tool belt -
we can get snapshots of values at any point, and we can use this information to
figure out if our function is doing what we expect it to do.  You can even pass
_functions_ into `console.log()` and it will log the result of the function!
Hmmmmmmm! 

## Moving Forward

That was a lot to cover! Let's do a quick recap:  

**Variables:**  
  * are like labelled boxes that contain a value
  * can be Strings (text), numbers, booleans, and other data types we will talk about later
  * are great for storing complex values, like result of complicated math, so we don't have to do the same work twice
  * once you assign a value to a variable, that variable **represents** the value, and can be used in the same ways that the value would be

**Functions:**
  * have a specific syntactic structure we need to follow when defining them
  * are called by writing their name, followed by parentheses (that may or may not have arguments)
  * are a set of actions, that, once the function is called, execute step by step
  * often return something, and their return value can be stored in a variable
  * aren't required to return anything - some functions do _something else_ inside, like call an alert, and don't need to return anything

Make sure to take time to practice writing variables and functions to get a better understanding of them.  

**NOTE:** For fun and a brain teaser, ask students what would happen if we were to try to assign a function _itself_ to a variable:

```js
function hello() {
  console.log("Hello, World!")
}

var myFunc = hello
// ???
```

## Resources
- [W3C — A Short History of JavaScript](https://www.w3.org/community/webed/wiki/A_Short_History_of_JavaScript)
- [Wikipedia — ECMAScript: Versions](https://en.wikipedia.org/wiki/ECMAScript#Versions)
- [MDN — Getting started with the Web](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web)
